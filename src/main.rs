use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::io::{BufRead, BufReader};
use clap::Parser;
use anyhow::{Context, Result};

// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
struct Cli {
    // The pattern to look for.
    pattern: String,
    // The path to the file to read.
    path: std::path::PathBuf
}

// Implement display for custom struct.
impl fmt::Display for Cli {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "pattern: {}, path: {}", self.pattern, self.path.as_path().display().to_string())
    }
}


fn main() -> Result<()> {
    // Start timer.
    let start = std::time::Instant::now();
    // Parse the command line args.
    let args = Cli::parse();
    // Print the command line args. We can do this because we implemented Display.
    println!("{}", args);

    // Open the file.
    let file = File::open(&args.path).with_context(|| format!("Could not read file `{}`", &args.path.as_path().display().to_string()))?;
    // Create a buffer reader.
    let mut reader = BufReader::new(file);
    // Iterator for line count.
    let mut i = 1;
    // Line buffer.
    let mut line = String::new();

    // Read the next line into the buffer.
    while reader.read_line(&mut line)? > 0 {
        // Check the current line for our pattern.
        if line.contains(&args.pattern) {
            println!("Line {}: {}", i, line.trim_end());
        }

        i += 1;
        // Clear the buffer for the next line.
        line.clear();
    }

    // End timer and calculate duration.
    let end = std::time::Instant::now();
    let duration = end - start;
    println!("Time elapsed: {:?}", duration);

    // Exit as planned.
    Ok(())
}
